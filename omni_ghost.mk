# Inherit APNs list
$(call inherit-product, vendor/omni/config/gsm.mk)

$(call inherit-product, device/motorola/ghost/full_ghost.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)


PRODUCT_NAME := omni_ghost
PRODUCT_DEVICE := ghost
PRODUCT_BRAND := motorola
PRODUCT_MODEL := Moto X
PRODUCT_MANUFACTURER := motorola
